let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('assets/js/src/app.js', 'assets/js/dist')
    .sass('assets/css/src/app.scss', 'assets/css/dist')
    .setResourceRoot('/themes/vaslv-minimalist/assets/');
